package com.yasiuraroman;

import com.yasiuraroman.t3.Task3;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import java.io.File;
import java.io.IOException;

public class Task3Tests {

    static final Logger logger = LogManager.getLogger();
    private Task3 task= new Task3(logger);

    @Test
    public void crateFileTest() throws IOException {
        try {
            task.createFileWithSize2MB();
        } catch (IOException e) {
            logger.error("Some Error");
        }
        File f = new File(Task3.FILE_PATH);
        TestCase.assertTrue(f.exists() && !f.isDirectory());
    }
}
