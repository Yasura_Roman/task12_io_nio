package com.yasiuraroman;

import com.yasiuraroman.serialize.Droid;
import com.yasiuraroman.serialize.Ship;
import com.yasiuraroman.serialize.ShipSerializer;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class SerializationTests {

    private Logger logger = LogManager.getLogger();

    @Test
    public void serializationTest(){
        List<Droid> droids = Arrays.asList(new Droid[]{
                new Droid("firsDroid", 10, 2, 2, 13),
                new Droid("secondDroid", 20, 2, 2, 10)
        });
        Ship ship = new Ship("firsShip", droids, 50, 100, 10,10 );

        ShipSerializer serializer = new ShipSerializer(logger);
        serializer.serializeShip(ship);
        Ship deserializeShip = serializer.deserializeShip();
        TestCase.assertEquals(ship.getName(), deserializeShip.getName());
    }
}
