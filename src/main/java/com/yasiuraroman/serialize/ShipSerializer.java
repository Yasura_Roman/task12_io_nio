package com.yasiuraroman.serialize;

import org.apache.logging.log4j.Logger;

import java.io.*;

public class  ShipSerializer {
    private static final String FILE_PATH = "./tmp/ship.ser";
    private static Logger logger;

    public ShipSerializer(Logger logger) {
        this.logger = logger;
    }

    public void serializeShip(Ship ship){
        try (
                FileOutputStream fileOut = new FileOutputStream(FILE_PATH);
                ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)
                ) {
            objectOut.writeObject(ship);
            logger.info("ship serialized in " + FILE_PATH);
        } catch (IOException e) {
            logger.error(e.getMessage());
            logger.error(e.getStackTrace());
        }
    }

    public Ship deserializeShip(){
        Ship ship = null;
        try (
                FileInputStream fileIn = new FileInputStream(FILE_PATH);
                ObjectInputStream objectIn = new ObjectInputStream(fileIn)
                ) {
            ship = (Ship) objectIn.readObject();
        } catch ( IOException | ClassNotFoundException e) {
            logger.error(e.getMessage());
            logger.error(e.getStackTrace());
        }
        return ship;
    }
}
