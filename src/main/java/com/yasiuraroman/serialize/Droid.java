package com.yasiuraroman.serialize;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    private int armorAmount;
    private int weaponsAmount;
    private transient int teamNumber;
    private int weaponsPower;

    public Droid(){

    }

    public Droid(String name, int armorAmount, int weaponsAmount, int teamNumber, int weaponsPower) {
        this.name = name;
        this.armorAmount = armorAmount;
        this.weaponsAmount = weaponsAmount;
        this.teamNumber = teamNumber;
        this.weaponsPower = weaponsPower;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getArmorAmount() {
        return armorAmount;
    }

    public void setArmorAmount(int armorAmount) {
        this.armorAmount = armorAmount;
    }

    public int getWeaponsAmount() {
        return weaponsAmount;
    }

    public void setWeaponsAmount(int weaponsAmount) {
        this.weaponsAmount = weaponsAmount;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        this.teamNumber = teamNumber;
    }

    public int getWeaponsPower() {
        return weaponsPower;
    }

    public void setWeaponsPower(int weaponsPower) {
        this.weaponsPower = weaponsPower;
    }
}
