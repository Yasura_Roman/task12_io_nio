package com.yasiuraroman.serialize;

import java.io.Serializable;
import java.util.List;

public class Ship implements Serializable {
    private String name;
    private List<Droid> droids;
    private int armorAmount;
    private int energyAmount;
    private int weaponAmount;
    private int WeaponPower;

    public Ship() {
    }

    public Ship(String name, List<Droid> droids, int armorAmount,
                int energyAmount, int weaponAmount, int weaponPower) {
        this.name = name;
        this.droids = droids;
        this.armorAmount = armorAmount;
        this.energyAmount = energyAmount;
        this.weaponAmount = weaponAmount;
        WeaponPower = weaponPower;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Droid> getDroids() {
        return droids;
    }

    public void setDroids(List<Droid> droids) {
        this.droids = droids;
    }

    public int getArmorAmount() {
        return armorAmount;
    }

    public void setArmorAmount(int armorAmount) {
        this.armorAmount = armorAmount;
    }

    public int getEnergyAmount() {
        return energyAmount;
    }

    public void setEnergyAmount(int energyAmount) {
        this.energyAmount = energyAmount;
    }

    public int getWeaponAmount() {
        return weaponAmount;
    }

    public void setWeaponAmount(int weaponAmount) {
        this.weaponAmount = weaponAmount;
    }

    public int getWeaponPower() {
        return WeaponPower;
    }

    public void setWeaponPower(int weaponPower) {
        WeaponPower = weaponPower;
    }
}
