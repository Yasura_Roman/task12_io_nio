package com.yasiuraroman.t6;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirectoryContent {

    public List<String> showDirectoryContent(String directory) {
        File dir = new File(directory);
        List<String> files = new ArrayList<>();
        getDirectoryContent(dir, files, "-");
        return files;
    }

    private void getDirectoryContent(File directory, List<String> files, String tab) {
        for (File file: directory.listFiles()) {
            files.add(tab + file.getName());
            if (file.isDirectory()) {
                getDirectoryContent(file, files, tab+"-");
            }
        }
    }

    public List<String> showDirectoryContentNIO(String directory) throws IOException {
        File dir = new File(directory);
        List<String> files;
        try (Stream<Path> walk = Files.walk(Paths.get(directory))) {
            files = walk.filter(Files::isRegularFile).map(f -> f.toString()).collect(Collectors.toList());
        }
        return files;
    }
}
