package com.yasiuraroman.t3;

import org.apache.logging.log4j.Logger;

import java.io.*;

public class Task3 {

    public static final String FILE_PATH = "./tmp/test_file.txt";
    public static final int MB = 1024 *1024;
    private Logger logger;

    public Task3(Logger logger) {
        this.logger = logger;
    }

    public void notBufferedRead() throws IOException {
        int count = 0;
        logger.info("notBufferRead");
        InputStream inputStream = new FileInputStream(FILE_PATH);
        int data = inputStream.read();
        while (data != -1){
            data = inputStream.read();
            count++;
        }
        inputStream.close();
        logger.info("count = " + count);
        logger.info("notBufferRead end");
    }

    public void bufferedReadWithBufferSize(int bufferSize) throws IOException {

        if (bufferSize < 1){
            logger.error("buffer size must be more by zero");
            throw new RuntimeException("buffer size must be more by zero");
        }

        int count = 0;
        logger.info("bufferedReadWithBufferSize " + bufferSize);
        DataInputStream dataInputStream = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(FILE_PATH),bufferSize));
        try {
            while (true){
                byte b = dataInputStream.readByte();
                count++;
            }
        } catch (EOFException e){
        }

        dataInputStream.close();
        logger.info("count = " + count);
        logger.info("bufferedReadWithBufferSize " +bufferSize+ " end");
    }

    public void createFileWithSize2MB() throws IOException {
        logger.info("createFileWithSize2MB");
        OutputStream outputStream = new FileOutputStream(FILE_PATH);
        for (int i = 0; i < 2 * MB; i++) {
            outputStream.write('a' + i % 5);
        }
        outputStream.flush();
    }
}
