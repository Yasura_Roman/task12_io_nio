package com.yasiuraroman.controller;

import com.yasiuraroman.serialize.Ship;
import com.yasiuraroman.serialize.ShipSerializer;
import com.yasiuraroman.t3.Task3;
import com.yasiuraroman.t4.InputOutStream;
import com.yasiuraroman.t5.ReadComment;
import com.yasiuraroman.t6.DirectoryContent;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class ControllerImp implements MyController {

    ShipSerializer serializer;
    Logger logger;

    public ControllerImp(Logger logger) {
        this.logger =logger;
        serializer = new ShipSerializer(logger);
    }

    @Override
    public void serializeShip(Ship ship) {
        serializer.serializeShip(ship);
    }

    @Override
    public Ship deserializeShip() {
        return serializer.deserializeShip();
    }

    @Override
    public void readWithDifferentBufferSize() throws IOException {
        Task3 model = new Task3(logger);
        model.notBufferedRead();
        model.bufferedReadWithBufferSize(128);
        model.bufferedReadWithBufferSize(256);
        model.bufferedReadWithBufferSize(512);
        model.bufferedReadWithBufferSize(1024);
    }

    @Override
    public void t4() throws FileNotFoundException {
        InputOutStream inputOutStream = new InputOutStream(new FileInputStream(new File("./tmp/test_file.txt")), 128);
    }

    @Override
    public List<String> readJavaSC(String fileName) throws IOException {
        ReadComment readComment = new ReadComment();
        return readComment.getAllComments(readComment.readFile(fileName));
    }

    @Override
    public List<String> showDirectoryContent(String directory) throws IOException {
        DirectoryContent directoryContent = new DirectoryContent();
        return directoryContent.showDirectoryContent(directory);
    }
}
