package com.yasiuraroman.controller;

import com.yasiuraroman.serialize.Ship;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface MyController {
    void serializeShip(Ship ship);
    Ship deserializeShip();
    void readWithDifferentBufferSize() throws IOException;
    void t4() throws FileNotFoundException;
    List<String> readJavaSC(String fileName) throws IOException;
    List<String> showDirectoryContent(String directory) throws IOException;
}
