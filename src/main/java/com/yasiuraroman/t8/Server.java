package com.yasiuraroman.t8;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {

    private Logger logger = LogManager.getLogger();
    private ServerSocketChannel serverSocketChannel;
    private Selector selector;
    private InetSocketAddress hostAddress;
    private SelectionKey selectionKey;
    private ServerController controller;

    public static void main(String[] args) throws IOException {
        new Server().start();
    }

    public Server() throws IOException {
        serverSocketChannel = ServerSocketChannel.open();
        hostAddress = new InetSocketAddress("localhost", 8080);
        selector = Selector.open();
        serverSocketChannel.bind(hostAddress);
        serverSocketChannel.configureBlocking(false);
        int operations = serverSocketChannel.validOps();
        selectionKey = serverSocketChannel.register(selector, operations, null);
        controller = new ServerController();
    }
    public void start() throws IOException {
        while (true) {
            int numberOfSelectedKey = selector.select();
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectedKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey k = iterator.next();
                if (k.isAcceptable()) {
                    try {
                        SocketChannel clientSocket = serverSocketChannel.accept();
                        clientSocket.configureBlocking(false);
                        clientSocket.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                        controller.createUser(clientSocket);
                    } catch (IOException e) {
                        logger.error(e.getMessage());
                    }
                } else if (k.isReadable()){
                    try {
                        SocketChannel client = (SocketChannel) k.channel();
                        ByteBuffer buffer = ByteBuffer.allocate(256);
                        client.read(buffer);
                        String output = new String(buffer.array()).trim();
                        logger.info("Message read from client" +client.getRemoteAddress() +": " + output);
                        controller.processCommand(output, client.getRemoteAddress().toString());

                        if (output.equals("exit")) {
                            client.close();
                            logger.info(client.getRemoteAddress() + " exit");
                        }
                    } catch (IOException e) {
                        logger.error(e.getMessage());
                    }
                }
                iterator.remove();
            }
        }
    }
}
