package com.yasiuraroman.t8;

import java.nio.channels.SocketChannel;

public class User {
    private SocketChannel socketChannel;
    private String address;
    private String name;


    public User(String address) {
        this.address = address;
    }

    public User(String address, String name) {
        this.address = address;
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User(SocketChannel socketChannel, String address) {
        this.socketChannel = socketChannel;
        this.address = address;
    }

    public SocketChannel getSocketChannel() {
        return socketChannel;
    }

    @Override
    public String toString() {
        return "User{" +
                "address='" + address + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
