package com.yasiuraroman.t5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReadComment {

    public String readFile(String sourceFilePath)  throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new FileReader(sourceFilePath));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line + "\n");
        }
        return sb.toString();
    }

    public List<String> getAllComments(String text) throws IOException {
        boolean flag = false;
        ArrayList<Character> characters = new ArrayList<>();
        List<String> comments = new ArrayList<>();
        for (int i = 0; i < text.length(); i++) {
            if (flag) {
                if (text.charAt(i) == '/') {
                    while (i < text.length() && text.charAt(i) != '\n') {
                        characters.add(text.charAt(i));
                        i++;
                    }
                } else if(text.charAt(i) == '*') {
                    while (i < text.length() && !(text.charAt(i) == '*' && text.charAt(i + 1) == '/')) {
                        characters.add(text.charAt(i));
                        i++;
                    }
                }
                comments.add(characters.stream().map(String::valueOf).collect(Collectors.joining()));
                characters.clear();
                flag = false;
                continue;
            }
            if (text.charAt(i) == '/') {
                flag = true;
                continue;
            }
        }
        return comments;
    }
}
