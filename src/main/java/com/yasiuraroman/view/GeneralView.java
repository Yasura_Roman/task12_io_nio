package com.yasiuraroman.view;

import com.yasiuraroman.controller.ControllerImp;
import com.yasiuraroman.controller.MyController;
import com.yasiuraroman.serialize.Droid;
import com.yasiuraroman.serialize.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class GeneralView {
    private MyController controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();

    public GeneralView() {
        controller = new ControllerImp(logger);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Serialize Ship");
        menu.put("2", "  2 - DeSerialize Ship");
        menu.put("3", "  3 - compere reading with different buffer size");
        menu.put("4", "  4 - Task 4");
        menu.put("5", "  5 - Read Java source-code");
        menu.put("6", "  6 - Content of directory");
        menu.put("7", "  7 - Task7");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", () -> {
            List<Droid> droids = Arrays.asList(new Droid[]{
                    new Droid("firsDroid", 10, 2, 2, 13),
                    new Droid("secondDroid", 20, 2, 2, 10)
            });
            Ship ship = new Ship("firsShip", droids, 50, 100, 10,10 );
            controller.serializeShip(ship);
        });
        methodsMenu.put("2", () -> {
            logger.info(controller.deserializeShip().toString());
        });
        methodsMenu.put("3", () -> {
            try {
                controller.readWithDifferentBufferSize();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        });
        methodsMenu.put("4", () -> {
            try {
                controller.t4();
            } catch (FileNotFoundException e) {
                logger.error(e.getMessage());
            }
        });
        methodsMenu.put("5", () -> {
            String file = input.nextLine();
            try {
                for (String s :controller.readJavaSC(file)
                     ) {
                    logger.info(s);
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        });
        methodsMenu.put("6", () -> {
            String directory = input.nextLine();
            try {
                for (String s :controller.showDirectoryContent(directory)
                ) {
                    logger.info(s);
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        });
        methodsMenu.put("7", () -> {

                logger.error("no implementation");
        });
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}

